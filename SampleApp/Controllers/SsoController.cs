﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

//  Extra dependencies
using System.Web;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace SampleApp.Controllers
{
    public class SsoController : Controller
    {
        //  Method to create the redirect URL, which includes the JWT SSO token
        private string GetRedirectURL(string returnTo = "")
        {

            //  Get the SSO Shared Secret from the Sisense Admin page
            string sharedSecret = "*****";

            //  Set the url of your Sisense server
            string sisenseServer = "http://takashi.sisense.com:80";

            //  Figure out the logged in user
            string myUser = "viewer@sisense.com";

            //  Figure out the timestamp for right now
            TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1));
            int timestamp = (int)t.TotalSeconds;

            //  Generate the JWT payload
            var payload = new Dictionary<string, object>() {
                 { "iat", timestamp},
                 { "sub", myUser},
                 { "jti", Guid.NewGuid() }
                 // Optional properties
                 // { "exp", (int)t.Add(TimeSpan.FromMinutes(30)).TotalSeconds  } // Expiration time
            };

            //  Create an encoder, that uses the SHA256 algorithm
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            //  Encode the JWT token payload, using the shared secret
            string token = encoder.Encode(payload, sharedSecret);

            //  Get the base of the redirect url
            string redirectUrl = sisenseServer + "/jwt?jwt=" + token;

            //  Check to see if a return_to parameter was passed in
            if (returnTo.Length > 0)
            {
                //  It was passed in, add it to the redirect url
                redirectUrl += "&return_to=" + HttpUtility.UrlEncode(sisenseServer) + HttpUtility.UrlEncode(returnTo);
            }

            //  Return the new url
            return redirectUrl;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            //  Get a reference to the current HTTP request
            string returnTo = this.HttpContext.Request.Query["return_to"].ToString(); ;

            //  Run the function to generate a redirect url
            string newUrl = GetRedirectURL(returnTo);

            //  Redirect this request to the new URL
            return new RedirectResult(newUrl);
        }
    }
}
