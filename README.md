# ASP.NET Core - SSO Sample

This project was created to show how single sign on can be setup with a .NET Core web application.  This basic example is based on the [tutorial](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/?view=aspnetcore-2.1) provided by Microsoft, and adds a controller for handling SSO.  

### Project Setup

## Step 1: Create the project
Open Visual Studio and create a new project.  The type of project used in this example was a ASP.NET Core Web App (MVC).

![Create Project](screenshots/1.new-project.png)

When configuring this project, give it a name like 'SampleApp'

![Configure Project](screenshots/2.configure-project.png)

## Step 2: Add the Dependencies
Right click on the Dependencies folder in Visual Studio, and click on the Add Packages button.  Find the _JWT_ package and add to the project.  It may prompt you to accept some license agreements, just click Yes on any popups.

![Add Dependency](screenshots/3.add-jwt-dependency.png)

## Step 3: Create a controller for SSO
![Add Dependency](screenshots/4.create-controller.png)

Create a new file and use the _MVC Controller Class_ as a template, and add some dependencies

```javascript
//  Extra dependencies
using System.Web;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
```

Create a private method to generate a redirect url
```javascript
//  Method to create the redirect URL, which includes the JWT SSO token
private string GetRedirectURL(string returnTo = "")
{

    //  Get the SSO Shared Secret from the Sisense Admin page
    string sharedSecret = "*****";

    //  Set the url of your Sisense server
    string sisenseServer = "http://takashi.sisense.com:80";

    //  Figure out the logged in user
    string myUser = "viewer@sisense.com";

    //  Figure out the timestamp for right now
    TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1));
    int timestamp = (int)t.TotalSeconds;

    //  Generate the JWT payload
    var payload = new Dictionary<string, object>() {
         { "iat", timestamp},
         { "sub", myUser},
         { "jti", Guid.NewGuid() }
         // Optional properties
         // { "exp", (int)t.Add(TimeSpan.FromMinutes(30)).TotalSeconds  } // Expiration time
    };

    //  Create an encoder, that uses the SHA256 algorithm
    IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
    IJsonSerializer serializer = new JsonNetSerializer();
    IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
    IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

    //  Encode the JWT token payload, using the shared secret
    string token = encoder.Encode(payload, sharedSecret);

    //  Get the base of the redirect url
    string redirectUrl = sisenseServer + "/jwt?jwt=" + token;

    //  Check to see if a return_to parameter was passed in
    if (returnTo.Length > 0)
    {
        //  It was passed in, add it to the redirect url
        redirectUrl += "&return_to=" + HttpUtility.UrlEncode(sisenseServer) + HttpUtility.UrlEncode(returnTo);
    }

    //  Return the new url
    return redirectUrl;
}
```

Now add an ActionResult to handle the HTTP request and return a redirection to the new url

```javascript
public IActionResult Index()
{
    //  Get a reference to the current HTTP request
    string returnTo = this.HttpContext.Request.Query["return_to"].ToString(); ;

    //  Run the function to generate a redirect url
    string newUrl = GetRedirectURL(returnTo);

    //  Redirect this request to the new URL
    return new RedirectResult(newUrl);
}
```

## Step 4: Add the route to the SSO controller
Now that we have a controller defined, we need a route that allows access to it.  Open _Startup.cs_ and look at the bottom for the app.UseMvc function, then add a new  MapRoute to the list of routes.
```javascript
//  Add the new route for your SSO controller
routes.MapRoute(
    name: "sso",
    template: "{controller=Sso}/{action=Index}");
```

